#!/bin/bash
set -e
set -x

./rebuild_drivers.sh
./load_modules.sh
./setup_devices.sh

DRIVER=${1:-418.56}

# Petri: These are needed to test that drivers work.
# Links cannot be absolute path because the folder is mounted and link would be broken
cd /opt/nvidia/lib64
ln -sf libnvidia-ml.so.$DRIVER libnvidia-ml.so
ln -sf libnvidia-ptxjitcompiler.so.$DRIVER libnvidia-ptxjitcompiler.so
ln -sf libnvidia-fatbinaryloader.so.$DRIVER libnvidia-fatbinaryloader.so
ln -sf libcuda.so.$DRIVER libcuda.so
ln -sf libcuda.so.$DRIVER libcuda.so.1
cd /

# These should be exported as env variables to work
export PATH=$PATH:/opt/nvidia/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/nvidia/lib64
/opt/nvidia/bin/nvidia-smi
