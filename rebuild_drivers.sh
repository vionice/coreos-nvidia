#!/bin/bash
set -e
set -x

if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit
fi


# Build
DRIVER=418.56
COREOS=$(sed --silent 's/^VERSION=\(.*\)$/\1/p' /usr/share/coreos/os-release)
CHANNEL=$(sed --silent 's/^GROUP=\(.*\)$/\1/p' /usr/share/coreos/update.conf)

./build.sh $DRIVER $CHANNEL $COREOS


# Cleanup old and move new libraries and modules to /opt
mkdir -p /opt/lib64/modules
mkdir -p /opt/nvidia/{lib64,bin}

rm -rf /opt/lib64/modules/nvidia*
rm -rf /opt/nvidia/lib64/*
rm -rf /opt/nvidia/bin/*

tar xvjf tools-$DRIVER.tar.bz2 -C /opt/nvidia/bin/
tar xvjf libraries-$DRIVER.tar.bz2 -C /opt/nvidia/lib64/
tar xvjf modules-$COREOS-$DRIVER.tar.bz2 -C /opt/lib64/modules/

if [ ! -f "/opt/lib64/modules/nvidia.ko" ]; then
    echo "Kernel modules do not exits in /opt/lib64/modules/"
    exit 1
fi

# symlinks
PREV_DIR=$(pwd)
cd /opt/nvidia/lib64
ln -s libcuda.so.$DRIVER libcuda.so
ln -s libnvidia-ml.so.$DRIVER libnvidia-ml.so
cd $PREV_DIR
